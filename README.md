# Desafio Desenvolvedor CDV #

Parabéns, você passou para o teste técnico do processo seletivo do Clube do Valor.

## Instruções ##

* Faça o Fork desse repositório e responda o desafio em um projeto com o seguinte nome: cdv-desafio-desenvolvedor-nome-sobrenome;
* Assim que concluir o seu desafio, publique o mesmo em sua conta do bitbucket ou github e mande o link do projeto para o recrutador, informando que finalizou.
* Não se esqueça de deixar o projeto como público para que possamos avaliá-lo
* Não esqueça de documentar o processo necessário para rodar a aplicação.
* Você pode utilizar a linguagem, componentes e frameworks que ficarem mais confortáveis para você.
* Quando achar necessário/útil, explique as decisões técnicas tomadas, as escolhas por bibliotecas e ferramentas, o uso de patterns etc. Você pode fazer isso em comentários no código, dentro de um arquivo do repositório, um screencast explicativo ou todas as alternativas anteriores.
* Soluções parciais serão aceitas. Caso algum ponto não esteja 100% em conformidade adicione um comentário no email ou README.md do repositório para nossa avaliação.
* A entrega deve seguir o prazo orientado pelo recrutador no e-mail
* Sinta-se a vontade para adicionar funcionalidades ao desafio se quiser

Então vamos lá!

## Desafio ##

Neste repositório está o arquivo com a série histórica de cotações da bolsa de valores para o mês de janeiro de 2021, juntamente com a documentação de como interpretar o arquivo. 
Utilize esses arquivos como fonte de dados para a sua aplicação ou seu banco de dados.

_Essas informações foram retiradas do seguinte link: http://www.b3.com.br/pt_br/market-data-e-indices/servicos-de-dados/market-data/historico/mercado-a-vista/cotacoes-historicas/_

O seu objetivo hoje é: 
Criar uma aplicação web onde um usuário possa fornecer os seguintes dados:

* Código de negociação de um papel
* Data do pregão

E então ver em tela:

* Preço do último negócio do papel naquele dia.

A aplicação tem apenas uma página e não necessita de controle de acesso. 

### Exemplo ###

Utilizando os dados anexados no repositório, quando o usuário informar o código PETR3 com a data 19/01/2021, deve ser retornado o valor R$ 29,12.

### Dicas e Constraints ###

* Considere que sua aplicação será um sucesso e um histórico muito maior de dados pode ser utilizado para alimentá-la.
* Controle de erros é importante, pois sua aplicação será utilizada por clientes finais.
* A interface não precisa ter um design impecável, mas mostre suas habilidades em front-end.
* Chamadas de API são interessantes para fornecer dados extras que o front-end não mostra.

## O que será avaliado ##

* O código será avaliado seguindo os seguintes critérios: estrutura, clareza e limpeza de código; resultado funcional; entre outros fatores.
* O histórico de commits também pode ser avaliado.
* O frontend não precisa ser impecável. Funcionalidade antes de beleza. Iremos avaliar principalmente os conhecimentos utilizados na construção do frontend.
* Caso ache interessante, explique melhorias que poderiam ser feitas. Isso pode ser feito em um arquivo dentro do repositório ou no email de resposta que nos enviar.

## Mostre o seu diferencial para a gente! ##

Estes pontos são opcionais, mas podem diferenciar você dos outros candidatos.

* Docker;
* Análise de dados;
* Testes automatizados;
* Documentação;
* Utilização de boas práticas;
* Design de frontend;


Boa sorte!